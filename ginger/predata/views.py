from django.http import HttpResponse
from django.shortcuts import render, get_list_or_404

from .models import PeopleProfile


def general(request):
    """
    Функция представления. Обязательно должна возвращать HttpResponse!
    (ну или юзать рендер)
    :param request:
    :return: HttpResponse
    """
    people = get_list_or_404(PeopleProfile)
    # template = loader.get_template('general.html')
    context = {
        'people': people,
    }
    # return HttpResponse(template.render(context))
    return render(request, 'general.html', context)


def result(request):
    return HttpResponse('This is result page')


def loaddata(request):
    return HttpResponse('This is loaddata page.')

def contacts(request):
    return HttpResponse("This is contact's page")

def faq(request):
    return HttpResponse('This is faq-page')
