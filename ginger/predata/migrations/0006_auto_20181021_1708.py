# Generated by Django 2.1.1 on 2018-10-21 14:08

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('predata', '0005_auto_20181021_1448'),
    ]

    operations = [
        migrations.RenameField(
            model_name='recordsfromwall',
            old_name='owner_id',
            new_name='owner',
        ),
    ]
