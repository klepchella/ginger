from django.db import models

SEX = (
    ('0', 'UNKNOWN'),
    ('1', 'WOMAN'),
    ('2', 'MAN')
)


class Faculty(models.Model):
    faculty = models.PositiveIntegerField(verbose_name='ID факультета', primary_key=True)
    faculty_name = models.CharField(verbose_name='Название', blank=True, max_length=300)


class University(models.Model):
    university_name = models.CharField(max_length=500, blank=True)
    faculty = models.PositiveIntegerField(verbose_name='ID факультета', null=True)
    graduation = models.PositiveIntegerField(verbose_name='Год окончания', null=True)


class School(models.Model):
    name = models.CharField(max_length=400, default='Не указано')
    country = models.PositiveIntegerField(blank=True, default=1) #todo: 1 - это же РФ?
    city = models.PositiveIntegerField(blank=True, default=0)
    year_from = models.PositiveSmallIntegerField(blank=True, default=0)
    year_to = models.PositiveSmallIntegerField(blank=True, default=0)
    year_graduated = models.PositiveSmallIntegerField(blank=True, default=0)
    class_name = models.CharField(max_length=2, default='', blank=True)
    type = models.PositiveSmallIntegerField(blank=True, default=0)
    type_str = models.CharField(max_length=1000, blank=True)


class Occupation(models.Model):
    type = models.CharField(verbose_name='Тип', max_length=50)
    name = models.CharField(verbose_name='Название', max_length=1000, blank=True, default='')


class PeopleProfile(models.Model):
    uid = models.PositiveIntegerField(verbose_name='ID пользователя ВК', primary_key=True)
    last_name = models.CharField(verbose_name='Фамилия', max_length=500, blank=True, default='')
    first_name = models.CharField(verbose_name='Имя', max_length=500, blank=True, default='')
    bdate = models.DateField(verbose_name='Дата рождения')
    sex = models.SmallIntegerField(verbose_name='Пол', choices=SEX)
    # education = models.ForeignKey(University, related_name='education', verbose_name='Образование', null=True,
    #                               blank=True, on_delete=models.PROTECT)
    city_name = models.CharField(verbose_name='Город', max_length=100, blank=True, default='')
    about = models.TextField(verbose_name='О себе', max_length=10000, blank=True, default='')
    occupation = models.ForeignKey(Occupation, verbose_name='Род деятельнонсти', blank=True, null=True,
                                   on_delete=models.PROTECT)
    schools = models.ManyToManyField(School, verbose_name='Школы', blank=True)
    universities = models.ManyToManyField(University, related_name='university', verbose_name='Образование', blank=True)


class RecordsFromWall(models.Model):
    owner = models.ForeignKey(PeopleProfile, verbose_name='ID пользователя ВК', on_delete=models.PROTECT)
    record_id = models.IntegerField(verbose_name='ID записи', primary_key=True)
    text = models.TextField(verbose_name='Текст', max_length=10000)
