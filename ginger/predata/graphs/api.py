import os
from pathlib import Path
from typing import List, Text
from uuid import uuid4

import matplotlib.pyplot as plt


# ~-------------------------~------------------------~-------------------------~
#                            Модуль для построения графиков
# ~-------------------------~------------------------~-------------------------~


class Canvas(object):

    def __init__(self, title=''):
        font = {'family': 'serif',
                'color': 'darkred',
                'weight': 'normal',
                'size': 16,
                }

        self.figure = plt.figure(
            dpi=300,
            figsize=(11, 8)
        )
        # Shrink current axis by 20%
        ax = plt.subplot(111)
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

        plt.minorticks_on()
        plt.title(title, fontdict=font)

        plt.grid(
            which='major', linestyle='-', linewidth='0.5', color='black')
        # Customize the minor grid
        plt.grid(
            which='minor', linestyle=':', linewidth='0.5', color='gray')

    @staticmethod
    def draw_plots(plot_name: Text, predict: List, answer: List,
                   x_label, y_label, prefix_name=''):
        """
        Графическое отображение результатов предсказанных в сравнении с
        "оригинальными".
        :param plot_name:
        :param predict:
        :param answer:
        :param x_label:
        :param y_label:
        :param prefix_name:
        :return:
        """

        color = 'red'
        size = 10
        font = {'family': 'serif',
                'color': 'darkred',
                'weight': 'normal',
                'size': 16,
                }

        if prefix_name:
            file_name = f'{prefix_name}_{plot_name}_{uuid4()}.png'
        else:
            file_name = f'{plot_name}_{uuid4()}.png'

        plt.xlabel(x_label, fontdict=font)
        plt.ylabel(y_label, fontdict=font)

        for list_, name in zip([predict, answer], ['Предсказание', 'Ответ']):

            x_point = list(i for i in range(len(list_)))
            y_point = list(i for i in list_)
            print(f'len({plot_name}) = {len(list_)}')

            plt.scatter(x_point, y_point, c=color, s=size, label=name)
            plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))

            color = 'blue'
            size = 3
        file_path = Path(__file__).parent
        dest_dir = os.path.join(file_path, 'image', plot_name)

        if not os.path.exists(dest_dir):
            os.mkdir(os.path.join(file_path, 'image', plot_name))

        destination_dir = os.path.join(file_path, 'image', plot_name,
                                       file_name)
        plt.savefig(
            destination_dir,
            quality=100,
            orientation='landscape',
            dpi=300
        )

        plt.show()

    @staticmethod
    def draw_plot(plot_name: Text, x_list, y_list, x_label, y_label,
                  prefix_name='', use_plot=False,
                  use_legend=False, legend=''):
        """
        Стандартный график y = f(x)
        :param plot_name:
        :param x_list:
        :param y_list:
        :param x_label:
        :param y_label:
        :param prefix_name:
        :param use_plot:
        :param use_legend:
        :param legend:
        :return:
        """
        color = 'red'
        size = 5
        font = {'family': 'serif',
                'color': 'darkred',
                'weight': 'normal',
                'size': 16,
                }

        if prefix_name:
            file_name = f'{prefix_name}_{plot_name}_{uuid4()}.png'
        else:
            file_name = f'{plot_name}_{uuid4()}.png'

        plt.title(plot_name, fontdict=font)
        plt.xlabel(x_label, fontdict=font)
        plt.ylabel(y_label, fontdict=font)
        plt.xticks(rotation=45)

        for x, y in zip(x_list, y_list):

            if use_legend:
                plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
                plt.scatter(x, y, c=color, s=size, name=legend)
            else:
                plt.scatter(x, y, c=color, s=size)
            if use_plot:
                plt.plot(x, y, linewidth=1, c=color)

        file_path = Path(__file__).parent
        dest_dir = os.path.join(file_path, 'image', plot_name)

        if not os.path.exists(dest_dir):
            os.mkdir(os.path.join(file_path, 'image', plot_name))

        destination_dir = os.path.join(file_path, 'image', plot_name,
                                       file_name)
        plt.savefig(
            destination_dir,
            quality=100,
            orientation='landscape',
            dpi=300
        )

        plt.show()
