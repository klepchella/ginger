from enum import IntEnum
from uuid import uuid4


class AgeGroupEnum(IntEnum):
    """
    •	От 0 до 10 лет, (1)
    •	От 11 до 16 лет, (2)
    •	От 17 до 20 лет, (3)
    •	От 21 до 25 лет, (4)
    •	От 25 до 35 лет, (5)
    •	От 36 до 45 лет, (6)
    •	От 46 до 90 лет. (7)

    """
    ONE = 10
    # TWO = 14
    THREE = 17
    FOUR = 20
    FIVE = 23
    # SIX = 26
    SEVEN = 29
    # EIGHT = 32
    # NINE = 35
    TEN = 39
    # ELEVEN = 39
    # TWELVE = 44
    # THIRTEEN = 46
    FOURTEEN = 50
    # FIFTEEN = 53
    # SIXTEEN = 56
    SEVENTEEN = 60
    # EIGHTEEN = 65
    NINETEEN = 70
    TWENTY = 90

    @staticmethod
    def index(group):
        for idx, key in enumerate(AgeGroupEnum.keys()):
            if group == key:
                return idx

        return 0

    @staticmethod
    def keys():
        return list(AgeGroupEnum.__members__.keys())

    @staticmethod
    def values():
        return list(AgeGroupEnum.__members__.values())

    @staticmethod
    def items():
        return list(AgeGroupEnum.__members__.items())

    @staticmethod
    def destinate_age_group(age):
        for group, group_age in AgeGroupEnum.items():
            if age <= group_age:
                return group
        return 0

    @staticmethod
    def get_group_name():
        with open(f'./group_{uuid4()}.txt', 'w', encoding='utf8') as group:
            prev = 0
            for idx, el in enumerate(AgeGroupEnum.values()):
                group.write(f'Группа №{idx}: от {prev} до {el} лет\n')
                prev = el + 1

