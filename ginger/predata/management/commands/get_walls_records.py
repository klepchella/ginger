import json
from time import sleep

from django.core.exceptions import ObjectDoesNotExist
from django.core.management import BaseCommand
from requests import Session

from ginger.settings import BASE_DIR
from predata.models import PeopleProfile, RecordsFromWall

__author__ = 'N_Lavrentyeva'

REQ = Session()


def get_walls_records():
    """
    Для каждого человека получаем его записи со стены. Максимальное кол-во записей - 100
    :return:
    """
    last_record = RecordsFromWall.objects.order_by('owner_id', 'record_id').last()
    error_filename = f'{BASE_DIR}/data/errors.log'
    if last_record:
        lr_uid = last_record.owner_id
    else:
        lr_uid = 1
    last_uid = lr_uid
    critical_errors = []
    count = 100
    offset = 0
    step_count = 0
    access_token = [
        'aa9c6915aa9c6915aa9c6915b7aafccd45aaa9caa9c6915f0c08fddafbd396d1e45a16c',
        '72137d0d72137d0d72137d0d68727158c07721372137d0d28ace0d85c5c136f8198fb2b',
        '888275688882756888827568ff88e0531a8888288827568d23dcbe319a0784fbabbae5f',
        '4bc9b6934bc9b6934bc9b693324bab9e8d44bc94bc9b6931109d381b9dfe9b5e12c2f0e',
        '4baf5f0e4baf5f0e4baf5f0ea24bcd772e44baf4baf5f0e116f3ac8d5da5777a6d212f1',
        'd49527d2d49527d2d49527d2bdd4fc5077dd495d49527d288020ae68abb3615b95c799a',
        '78b24afb78b24afb78b24afb9a78d834a9778b278b24afb2467eb091ba064c9742d2fdc',
        'f71e4accf71e4accf71e4acce6f7743498ff71ef71e4accabcbe8ea7f9d74ff2e1bcb80',
        '931ed683931ed683931ed683ba9374a8d69931e931ed683cfcb74d79838e59862ae33ab',
        'b8260c47b8260c47b8260c4796b84c89c0bb826b8260c47e4f1e4ed1e4d84f4f2f15cbf',
        'bb5fc8e1bb5fc8e1bb5fc8e14abb354d69bbb5fbb5fc8e1e788201459855f32ab9f4507',
        'd414d0d8d414d0d8d414d0d8c8d47e5553dd414d414d0d888c339f97131db09367999c9',
        'bfbc283dbfbc283dbfbc283deabfd6a40ebbfbcbfbc283de365b0cd95098087e3326f10',
        '3cf233d73cf233d73cf233d7f13c9891a733cf23cf233d7601233f5ee11af978fede560',
        '890c9528890c9528890c95289c8966375b8890c890c9528d5ec95bbd98c08f715e5cd6e',
        '1fbcd8991fbcd8991fbcd899ff1fd67aef11fbc1fbcd899435cd83246cc2ee0585a83ca',
        'de31ae8dde31ae8dde31ae8d78de5b0dd1dde31de31ae8d82d19f5d8e01da3d94c56f7d',
        'a7cb5ee5a7cb5ee5a7cb5ee5a4a7a1fdbaaa7cba7cb5ee5fb2b6f143f048f1b4f735ca4',
        '167971f9167971f9167971f9d21613d29811679167971f94a9943f1142103ce66da8175',
    ]
    id_acc_token = 0
    while not critical_errors and step_count <= 3:
        users_ids = list(PeopleProfile.objects.order_by('uid').filter(uid__gt=last_uid).values_list('uid', flat=True)[
                    :100])
        try:
            if users_ids:
                last_uid = users_ids[-1]
            else:
                # offset += 100
                step_count += 1
                # raise SystemExit
            for id_ in users_ids:
                try:
                    # TODO: добавить проверку кол-ва записей от одного
                    #  человека - в идеале их должно быть больше сотни
                    response = REQ.get('https://api.vk.com/method/wall.get',
                                       params={'owner_id': id_,
                                               'offset': offset,
                                               'count': count,
                                               'filter': 'owner',
                                               'version': '5.87',
                                               'access_token': access_token[id_acc_token]})
                    error = process_response(response, id_)
                    if error:
                        # если есть какие-то ошибки, то что-то с ними делаем
                        with open(error_filename, 'a') as error_file:
                            err_code = error['error_code']
                            if err_code in [6, 9]:
                                if id_acc_token < len(access_token):
                                    id_acc_token += 1
                                    print('Too many requests! We change the app.\n')
                                    continue
                                else:
                                    print('Too many requests! We\'re sleeping\n')
                                    sleep(86401)
                                    continue
                            elif err_code in [1, 7, 10, 15, 16, 17, 18, 200, 201]:
                                continue
                            elif err_code in [2, 3, 4, 8, 20, 21, 23, 28, 100]:
                                print('Internal error! Fix error and restart script.')
                                err = f'Critical error code: {err_code}, user_id={id_}\n'
                                error_file.write(err)
                                critical_errors.append(error)
                            else:
                                print('ALARM!!! Everything was crashed!!')
                                err = f'Critical error code: {err_code}, user_id={id_}\n'
                                error_file.write(err)
                                critical_errors.append(error)
                except ConnectionError:
                    print('Connection error! id: ', id_)
                    sleep(5)
                except FileNotFoundError:
                    print('Error file not found!')
                    continue
                except:
                    print('Something wrong! id: ', id_)
                    raise SystemExit
        except (KeyError, IndexError):
            pass


def process_response(response, user_id):
    """
    Обработка полученного от сервера ответа.
    :param response: json
    :return:
    """
    response = json.loads(response.text)
    errors = response.get('error', [])
    try:
        posts = response.get('response', [])
    except AttributeError:
        return errors

    for post in posts[1:]:
        try:
            record = RecordsFromWall.objects.get(
                owner_id=post.get('owner_id', user_id),
                record_id=post.get(
                'record_id', post['id']))
        except ObjectDoesNotExist:
            record = RecordsFromWall()
            record.record_id = post.get('record_id', post['id'])
            record.owner_id = post.get('owner_id', user_id)
            if post['post_type'] == 'post' and post['text']:
                record.text = post['text']
            elif post['post_type'] == 'copy' and post.get('copy_text', False):
                record.text = post.get('copy_text')
            else:
                continue
            record.save()
    return errors


class Command(BaseCommand):
    def handle(self, *args, **options):
        get_walls_records()
