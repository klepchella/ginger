import csv
import re
from collections import namedtuple
from datetime import date
from math import fabs
from string import punctuation
from typing import Generator, List
from uuid import uuid4

import nltk
import numpy as np
from django.core.management import BaseCommand
from django.db import connection
from nltk import word_tokenize
from pymorphy2 import MorphAnalyzer
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import SGDClassifier
from sklearn.model_selection import train_test_split, cross_val_score
# многослойный перцептон
from sklearn.neural_network import MLPClassifier
# метод опорных векторов
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

from predata.enums import AgeGroupEnum
from predata.graphs.api import Canvas

nltk.download('stopwords')
nltk.download('punkt')
from nltk.corpus import stopwords
russian_stopwords = stopwords.words("russian")
english_stopwords = stopwords.words("english")


__author__ = 'N_Lavrentyeva'


class RawData(object):

    stop_list = []
    stop_list.extend(russian_stopwords)
    stop_list.extend(english_stopwords)
    stop_list.append(['group_link', 'man_link', 'internal_link',
                      'digits', '—', '–', ' '])

    def __init__(self):
        self.morph = MorphAnalyzer()

    @staticmethod
    def get_records(limit=10, count=10):
        """
        Сырой sql_template-запрос, который агрегирует все тексты почеловечно.
        По идее, дальше загнать это либо в
        dataframe/json/csv
        (последнее уже самый крайний случай - не надо с файлами возиться)
        и уже работать с такими данными.
        :return:
        """

        Data = namedtuple('Data', ['uid', 'bdate', 'walls_text'])
        sql = (f"DROP FUNCTION if exists get_records(integer,integer);"
               f"create or replace function get_records("
               f"in count_record integer, in records_limit integer)"
               f"returns table(	id_ int, bdate date, text_ text) as "
               f"$body$ "
               f"declare "
               f"id_ int;"
               f"begin "
               f"for id_ in select uid from predata_peopleprofile limit "
               f"count_record loop "
               f"    return query (select distinct uid, "
               f"predata_peopleprofile.bdate, array_to_string(array(select "
               f"predata_recordsfromwall.text from predata_recordsfromwall where"
               f" owner_id = id_ order by uid limit records_limit ), ' ') "
               f"from predata_recordsfromwall join predata_peopleprofile on "
               f"uid=owner_id where uid=id_);	"
               f"end loop; "
               f"return;"
               f"end;"
               f"$body$ LANGUAGE plpgsql;"
               f"select id_, bdate, text_ from get_records({count}, {limit}) "
               f"as foo "
               f"where text_ <> '';")

        with connection.cursor() as cursor:
            cursor.execute(sql)
            rows = cursor.fetchall()
            data = Data(uid=[row[0] for row in rows],
                        bdate=[row[1] for row in rows],
                        walls_text=[row[2] for row in rows])

        return data

    @classmethod
    def remove_link(cls, data):
        # print('start clearing')
        data = re.sub('<br>', '', data)
        data = re.sub(
            'https?\:\/\/(www\.)?(\w+[\&\-\=\.\/\?]?)+',
            ' external_link ',
            data)

        data = re.sub('\[?club\d+\|(\w+ ?)+\]?', ' group_link ', data)
        data = re.sub('\[?id\d+\|(\W?\w+ ?\W?)+\]?', ' man_link ', data)
        data = re.sub('\[?wall\d+_?\d+\]?', ' internal_link ', data)
        data = re.sub('\d+', ' digits ', data)
        # print('after cleaning')
        return data

    def morph_analyze(self, words: Generator):
        # print('start morph analyze')
        text = ''
        normalized_words = []
        for word in words:
            parsed_info = self.morph.parse(word)[0]
            try:
                normilized = parsed_info.inflect({'sing', 'nomn'}).word
            except:
                normilized = parsed_info.normal_form
            normalized_words.append(normilized)
        text = ' '.join(normalized_words)
        # print('end morph analyze')
        return text

    def preprocess_text(self, features_data):
        """
        Обработка текстов - вычёркивание лишних слов, переносов и прочего.
        По идее, необходимо расширить russian_stopwords словами,
        которые попадаются с ОЧЕНЬ высокой частотой. Тот же <br>
        :param features_data:
        :return:
        """

        # приводим всё к строчными буквам, заменяем ссылки на стоп-слова
        # features_data = features_data.lower()
        data = self.remove_link(features_data)

        # words = word_tokenize(data)
        # выкидываем слова, входящие в стоп-лист
        # words_filtered = (word for word in words if (word not in stop_list and
        #                                              word not in punctuation))
        # генератор, чтобы не забивать память уж совсем
        text = self.morph_analyze(
            (word for word in word_tokenize(data) if (
                    word not in self.stop_list and word not in punctuation))
        )

        return text


class TrainModel(object):
    def __init__(self, features, target: List):
        self.features = features
        self.target = target

    def train(self, model):
        try:
            model.fit(self.features, self.target)
        except TypeError:
            model.fit(self.features.toarray(), self.target)

        return model

    @staticmethod
    def predict(model, new_features):
        try:
            preds = model.predict(new_features)
        except (ValueError, TypeError):
            try:
                preds = model.predict(new_features.toarray())
            except ValueError:
                return False
        except MemoryError:
            return False
        return preds


def write_errors(for_writing: List, filename='result', *args, **kwargs):
    with open(f'./{filename}_{uuid4()}.csv',
              'w+', encoding='utf-8') as pred_file:
        writer = csv.writer(
            pred_file, delimiter='\n', quotechar='|',
            quoting=csv.QUOTE_MINIMAL)
        for el in for_writing:
            if isinstance(el, dict):
                writer.writerow(el.items())
            elif isinstance(el, list) or isinstance(el, str):
                writer.writerow(el)


def classification():
    """
    Тренировочная работа с линейной моделью

    Какая стратегия моделирования больше всего подходит в выбранной ситуации?
    Возможны два подхода.
    1) Рассматривать каждый фильм как отдельный экземпляр,
    объединять все обзоры к нему и фиксировать их тональность
    как средний балл или как многоклассовую модель.
    2) В качестве экземпляров рассматриваются отдельные рецензии,
    каждая новая рецензия оценивается
    по своей положительности, а каждому новому фильму присваивается
    средний рейтинг.

    В данном случае логичнее всё-таки будет вариант 1)
    :return:
    """

    def calculate_sigma(predict: List, answer: List):
        sigma = []
        for x, y in zip(predict, answer):
            sigma.append(x - y)
        return sigma

    def calculate_age(bdate):
        today = date.today()
        return (today.year
                - bdate.year
                - ((today.month, today.day)
                   < (bdate.month, bdate.day)))

    # признаки - входные атрибуты, используемые
    # для предсказания целевой переменной

    # цель (метка) - атрибут, представляющий интерес;
    # переменная, которая предсказывается для каждого нового экземпляра

    # тестовые данные, для новых предсказаний

    # training data - набор экземпляров с известной
    # целевой переменной для подгонки модели

    # не менять, памяти хватает!
    count_records = 100000000
    limit = 50
    split_coef = 0.7
    rawdata = RawData()
    people_list = rawdata.get_records(count=count_records, limit=limit)

    ages = []
    texts = []
    print('after_get_records')
    count_people = 0
    AgeGroupEnum.get_group_name()
    for idx in range(len(people_list.uid)):
        if count_people > 3000:
            break
        age = calculate_age(people_list.bdate[idx])
        group = AgeGroupEnum.index(AgeGroupEnum.destinate_age_group(age))
        # убираем глобальные скачки, которые будут портить картину
        if 5 < age < 120:
            text = rawdata.preprocess_text(people_list.walls_text[idx])
            if len(text) >= 150:
                count_people += 1
                ages.append(group)
                # ages.append(age)
                texts.append(text)

    print(f'count people = {count_people}')
    print('after join text')

    # разбиваем случайным образом на обучающую/тестовую выборки
    (X_train, X_test,
     y_train, y_test) = train_test_split(
        texts,
        ages,
        test_size=1-split_coef,
        random_state=11
    )

    if X_train and X_test and y_train and y_test:
        #  "Здесь важно понимать, что словарь «мешка слов» нельзя
        #  загрязнять словами из тестовой выборки. Набор данных
        #  разбивается до построения векторизованного словаря именно для того,
        #  чтобы получить реалистичную оценку точности модели при работе
        #  с ранее неизвестными данными."
        #  (c) Машинное обучение, стр. 251, п. 8.2.1
        # todo: вполне вероятно, придётся делать отдельные непересекающиеся сеты
        #  по словам train и test

        # инициализируем вектор, подсчитывающий слова
        vectorizer = TfidfVectorizer()
        try:
            # обучает словарь и генерирует признаки для обучающей выборки
            # эта хрень векторизирует частоту слов, формируя признак вида (
            # слово-кол-во употребление). Сделано потому, что мы не можем в
            # качестве признаков использовать ГРЕБАНЫЙ ТЕКСТ!!
            features_cross_train = vectorizer.fit_transform(X_train)

            # генерирует признаки для тестового набора
            features_cross_test = vectorizer.transform(X_test)
        except ValueError:
            print('Недостаточно данных!')
            raise SystemError

        print('creating model')
        # создание модели

        model_cross = TrainModel(features_cross_train, y_train)

        model_mapping = {
            # 'GAUSS': GaussianProcessClassifier(
            #     copy_X_train=False,
            #     # n_jobs=-1
            # ),
            'SVC': SVC(
                gamma='auto',
                # degree=2,
            ),
            'SGDC': SGDClassifier(
                n_jobs=-1,
                # max_iter=100,
            ),
            'DecTree': DecisionTreeClassifier(
                # max_depth=100,
                criterion='gini',
                # max_leaf_nodes=50,
            ),
            # 'GaussNb': GaussianNB(),
            'RndFrst': RandomForestClassifier(
                # n_jobs=-1,
                # max_depth=90,
                # max_leaf_nodes=50
            ),
            'MPL': MLPClassifier(
                # max_iter=50,
            ),
            # 'RndFsrtR': RandomForestRegressor(
            #     n_jobs=-1,
            #     # max_depth=90,
            #     # max_leaf_nodes=50
            # )
        }

        sigma_mapping = dict(
                (model_name_, None) for model_name_ in model_mapping.keys()
        )

        correct_predict = dict(
                (model_name_, 0) for model_name_ in model_mapping.keys()
        )

        # Список добработок
        # Поиграться с параметрами (просмотреть и проверить
        # результаты с каждым из поменянных, собрать картинки):
        # •	n_estimators
        # •	criterion
        # •	max_features
        # •	min_samples_split
        # •	n_jobs (производительность)
        #
        means = {}
        scores_dict = {}
        for model_name, model_class in model_mapping.items():
            print(f'model: {model_name}')
            try:
                train_cross_model = model_cross.train(model_class)
                # предсказание новых данных
                predict = model_cross.predict(
                    train_cross_model,
                    features_cross_test,
                )
                try:
                    scores = cross_val_score(
                        estimator=train_cross_model,
                        X=features_cross_test,
                        y=y_test)
                except TypeError:
                    scores = cross_val_score(
                        estimator=train_cross_model,
                        X=features_cross_test.toarray(),
                        y=y_test)

                scores_dict.update({model_name: scores})
                means.update({model_name: np.mean(predict == y_test)})

            except MemoryError:
                print('Memory Error!')
                continue

            sigma_mapping[model_name] = calculate_sigma(scores,
                                                        y_test)

            correct_predict[model_name] = (
                    sum(fabs(el) for el in sigma_mapping[model_name])
                    / len(sigma_mapping[model_name]))
            write_errors([sigma_mapping, correct_predict])

            # рисуем график для каждого отдельного случая
            # подготавливаем график
            canvas = Canvas(model_name)
            canvas.draw_plots(
                # model_name, predict, y_test, 'Люди', 'Возраст (в годах)',
                model_name, predict, y_test, 'Люди', '№ группы',
            )

        write_errors([scores_dict], 'cross_val_score')
        canvas = Canvas('Точноть алгоритмов')
        canvas.draw_plot(
            'means', list(model_mapping.keys()), means.values(),
            'Алгоритм', 'Точность',
        )

    else:
        print('Недостаточно данных для обработки!')

    print('Done!')
    raise SystemExit


class Command(BaseCommand):
    def handle(self, *args, **options):
        classification()

