import datetime
from collections import defaultdict
from json import loads
from re import findall
from time import sleep

from django.core.management import BaseCommand
from django.db import IntegrityError
from requests import get

from predata.models import PeopleProfile, Occupation, School, University

__author__ = 'N_Lavrentyeva'


def send_request():
    """
    Отправить запрос в ВК.
    """
    last_man = PeopleProfile.objects.order_by('uid').last()
    if last_man:
        last_man_id = last_man.uid
    else:
        last_man_id = 1
    critical_errors = defaultdict()
    step = 100
    while not critical_errors:
        try:
            request = get('https://api.vk.com/method/users.get', params={
                'user_ids': str(list(range(last_man_id+1,
                                           last_man_id+step))).replace('[', '').replace(']', ''),
                'fields': 'sex,bdate,city,occupation,schools,about,education,universities',
                'version': '5.85',
                'access_token': 'aa9c6915aa9c6915aa9c6915b7aafccd45aaa9caa9c6915f0c08fddafbd396d1e45a16c'
            })
            last_man_id += step
        except ConnectionError:
            print('Error in get!')
            raise SystemExit
        else:
            server_response = loads(request.text)
            errors = server_response.get('error')
            response = server_response.get('response')

            if errors:
                print('Error code: ', errors['error_code'], '; user_id: ', last_man_id)
                if errors['error_code'] in [2, 3, 4, 8, 20, 21, 23, 28, 100]:
                    print('Internal error! Fix error and restart script.')
                    critical_errors[last_man_id] = errors['error_code']
                    raise SystemExit
                elif errors['error_code'] in [1, 7, 10, 15, 16, 17, 18, 200, 201]:
                    continue
                elif errors['error_code'] in [6, 9]:
                    print("Too many requests per second. I'm sleeping.")
                    sleep(86400)
                else:
                    print('ALARM!!! Everything was crashed!!')
                    raise SystemExit
            elif response:
                for rec in server_response['response']:
                    if rec.get('bdate', -1) != -1 and findall(r'\d{2}\.\d{1,2}.\d{4}', rec['bdate']):
                        man = create_man(rec)
                        for key in rec:
                            if key in ['occupation', 'schools', 'universities']:
                                create_dependencies(key, man, rec)
                                # man.occupation = occ
                            elif key in ['university', 'university_name']:
                                continue


def create_dependencies(key, object, record):
    """
    Создание "зависимых" объектов (или проверка на существование)
    :param key:
    :param object:
    :param record:
    :return:
    """

    # for el in record[key]:
    # может придти либо список из словарей, либо словарь
    if key == 'schools':
        for school_el in record[key]:
            school, created = School.objects.get_or_create(id=school_el['id'])
            if not created:
                school = School()
                for sch in school_el:
                    setattr(school, sch, school_el[sch])
                school.save()
            try:
                object.schools.add(school.id)
            except IntegrityError:
                continue
    elif key == 'universities':
        for universe in record[key]:
            university, created = University.objects.get_or_create(
                id=universe['id'])
            if not created:
                for sch in universe:
                    # TODO: а не надо ли это выпилить?
                    if sch == 'id':
                        continue
                    setattr(university, sch, universe[sch])
                university.save()
            try:
                object.universities.add(university)
            except IntegrityError:
                continue
    elif key == 'occupation':
        try:
            model_, created = Occupation.objects.get_or_create(
                type=record[key]['type'],
                name=record[key]['name'])
            object.occupation_id = model_.id
            object.save()
        except IntegrityError:
            pass


def create_man(record):
    """
    Создание человека
    :param record:
    :return:
    """
    man = PeopleProfile()
    fields_list = ['uid', 'last_name', 'first_name', 'sex', 'about']
    for field in fields_list:
        setattr(man, field, record.get(field, ''))

    try:
        man.city_name = record.get('city').get('title')
    except AttributeError:
        man.city_name = record.get('city')
    try:
        man.bdate = datetime.datetime.strptime(record['bdate'], '%d.%m.%Y').date()
    except ValueError:
        bdate = record['bdate'].split('.')
        month = (int(bdate[1]) + 1) % 13
        man.bdate = datetime.date(int(bdate[2]) + (bdate[1] == 12),
                                  month if month else month+1,
                                  1)

    man.save()
    return man


class Command(BaseCommand):
    def handle(self, *args, **options):
        send_request()
