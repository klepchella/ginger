from django.conf.urls import url
from . import views

__author__ = 'N_Lavrentyeva'


app_name = 'predata'
urlpatterns = [
    url(r'^$', views.general, name='general'),
    url(r'contacts/$', views.contacts, name='contacts'),
    url(r'loaddata/$', views.loaddata, name='loaddata'),
    url(r'result/$', views.result, name='result'),
    url(r'faq/$', views.faq, name='faq')
]
